# README for `genomic_refs` project

This lightweight project just sets reference datasets that are used in my other
projects. The reference datasets are not restricted to genome-builds, and
include resources for systems biology (eg, coxpresdb) that are likely to be
used by multiple projects.

The genome references that are used are

- Human

    - GRCh38 genomic reference

    - Ensembl v84 transcriptome reference

    - The HISAT2 reference that was constructed from GRCh38 and Ensembl v84

- Mouse

    - GRCm38

    - Ensembl v84 transcriptome reference

    - The HISAT2 reference that was constructed from GRCm38 and Ensembl v84

Downloads of individual GEO or ArrayExpress datasets (and other
project-restricted small datasets) should not be done from this project.

