###############################################################################
#
#
#
###############################################################################

import os.path
import re
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider

HTTP = HTTPRemoteProvider()
FTP = FTPRemoteProvider()

###############################################################################
# python code

###############################################################################
def get_hisat2_index_names():
    return expand("hisat2/genome_tran.{i}.ht2", i=range(1, 9))


def get_assembly_index_names(release_name):
    suffixes = [
        "amb", "ann", "bwt", "dict", "fa.gz", "fa.gz.fai", "fa.gz.gzi",
        "pac", "sa"
    ]
    indexes = expand(
        "assembly/{release_name}.{suff}",
        release_name=release_name,
        suff=suffixes
    )
    return indexes

def get_ensembl_suffixes(species, genome_build, transcriptome_build):
    return expand(
        "ensembl.v{tx}/{sp}.{gx}.{tx}.{ext}",
        sp=species,
        gx=genome_build,
        tx=transcriptome_build,
        ext=["gtf.gz", "protein_coding.bed12"]
    )

###############################################################################

# Directories:

ext_dir = os.path.join("data", "ext")
genome_dir = os.path.join(ext_dir, "genomes")
coxpresdb_dir = os.path.join(ext_dir, "coxpresdb")

###############################################################################

# Genome Files:

human_hisat2_suffixes = [
        "hisat2/make_grch38_tran.sh"
    ] + \
    get_assembly_index_names(
        "Homo_sapiens.GRCh38.dna.primary_assembly"
    ) + \
    get_hisat2_index_names()

mouse_hisat2_suffixes = [
        "hisat2/make_grcm38_tran.sh"
    ] + \
    get_assembly_index_names(
        "Mus_musculus.GRCm38.dna.primary_assembly"
    ) + \
    get_hisat2_index_names()

###############################################################################

human_bowtie2_files = [
    os.path.join(genome_dir, "GRCh38", "bowtie2", fname)
    for fname in config["GRCh38"]["bowtie2"]["contents"]
]

human_ensembl_files = [
    os.path.join(genome_dir, "GRCh38", fname)
    for fname in get_ensembl_suffixes("Homo_sapiens", "GRCh38", "84")
]

human_hisat2_files = [
    os.path.join(genome_dir, "GRCh38", fname)
    for fname in human_hisat2_suffixes
]

mouse_ensembl_files = [
    os.path.join(genome_dir, "GRCm38", fname)
    for fname in get_ensembl_suffixes("Mus_musculus", "GRCm38", "84")
]

mouse_hisat2_files = [
    os.path.join(genome_dir, "GRCm38", fname)
    for fname in mouse_hisat2_suffixes
]

# Coexpression Files:

human_coxpresdb = os.path.join(
  coxpresdb_dir,
  "Hsa.v13-01.G20280-S73083.rma.mrgeo.d.tar.bz2"
)
human_cox_top100 = re.sub(".tar.bz2", "_top100.tar.bz2", human_coxpresdb)
human_cox_top100_onefile = re.sub(".tar.bz2", "_onefile.gz", human_cox_top100)

###############################################################################
# snakemake code

# TODO: download the files using ftp
# TODO: move the downloaded files to
# ./data/genomes/GRCm38/[assembly|hisat2|ensembl]/
# OR:   write script to do all the downloads

# TODO: snakemake .config file mapping genomic build IDs to
# assembly/hisat2/ensembl URLs

rule all:
    input:
        human_hisat2_files, mouse_hisat2_files, human_bowtie2_files,
        human_ensembl_files, mouse_ensembl_files
#,
#        human_coxpresdb, human_cox_top100, human_cox_top100_onefile

###############################################################################

rule bowtie2:
    input:
        FTP.remote(
            config["GRCh38"]["bowtie2"]["ftp"], keep_local=False,
            immediate_close=True
        )

    output:
        human_bowtie2_files

    params:
        out_dir = lambda wildcards, output: os.path.dirname(output[0])

    shell:
        """
        tar -C {params.out_dir} -xzvf {input}
        """

rule grcm38:
    output:
        mouse_hisat2_files

    shell:
        "bash ./scripts/GRCm38.sh"

rule grch38:
    output:
        human_hisat2_files

    shell:
        "bash ./scripts/GRCh38.sh"

###############################################################################

rule coxpresdb:
    input:
       HTTP.remote(
           "coxpresdb.jp/download/coex/{basename}.tar.bz2",
           keep_local=True, insecure=True)
    output:
        os.path.join(coxpresdb_dir, "{basename}.tar.bz2")
    shell:
        "mv {input} {output}"

rule coxpresdb_top100:
    message:
        """
        Extract the top 100 partners for each gene in a coxpresdb archive and
        construct a new coxpresdb archive of the same structure.

        If this throws an error in Snakemake, there may be a bunzip2'ed archive
        sitting in the target directory that you should remove.
        """
    input:
        cox_raw = "{prefix}.tar.bz2",
        script = "scripts/coxpresdb_top100.sh"
    output:
        "{prefix}_top100.tar.bz2"
    shell:
        "./{input.script} {input.cox_raw} {output}"

# TODO: check why this prompts the user to overwrite the .gz file
# - i ran it at a point where the .gz shouldn't have existed and it prompted
# whether to overwrite or not.

# TODO: push the logic into the coxpresdb_top100.sh script

rule coxpresdb_top100_onefile:
    message:
        """
        Collapses the set of top-100 files for all source-genes in the
        coxpresdb archive.
        """
    input:
        "{prefix}_top100.tar.bz2"
    output:
        "{prefix}_top100_onefile.gz"
    shell:
        """
        for f in $(tar --list -f {input}); do

          if [[ "${{f}}" = */ ]]; then continue; fi

          gene=$(basename $f)
          tar --to-stdout -xf {input} $f |\
            head -n101 |\
            tail -n100 |\
            awk -v gene="$gene" 'BEGIN{{OFS="\t"}}{{print gene,$0}}'
        done \
        > {wildcards.prefix}_top100_onefile

        gzip {wildcards.prefix}_top100_onefile > {output}
        """

###############################################################################

include: "scripts/snake_recipes/ensembl_gtf.smk"
include: "scripts/snake_recipes/gtf_to_bed_ucsc.smk"

###############################################################################
