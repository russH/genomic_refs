----

# TODO for `genomic_refs` project

----

## `Snakefile`

- Puts output filenames into a config file

- Uses snakemake.remote.HTTP instead of bash download script for hisat etc
  downloads

- Splits Snakefile

    - makes `scripts/coxpresdb.smk`, `scripts/hisat_ref.smk`

    - calls the sub-snakes from Snakefile

- Work with coxpresdb .zip files instead of .tar.bz2 files

    - the files stored at coxpresdb have been modified slightly

## `scripts`

- `coxpresdb.smk`

    - adds this script

- `hisat_ref.smk`

    - adds this script

----

TIMELINE

----

# 20180307

## `Snakefile`

- Adds code to download coxpresdb.jp human dataset to `ext_data` using
  HTTP.remote function in snakemake

# 20170725

- BGZIP/FAIDX on the human reference

- BGZIP/FAIDX on the mouse reference

- bgzip-generated .gz file for the ensembl-obtained fastas

- samtools faidx index of the ensembl-obtained fastas

# 20170720

- Human GRCh38 genomic reference from HISAT2

- Human GRCh38 genomic reference from EBI

- Human GRCh38/84 transcriptome definition from Ensembl

