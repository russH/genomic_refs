#!/bin/bash -e

###############################################################################
# 2017-07-20
# - Script to download the human-specific genome / transcriptome assemblies
#   and references for use in hisat2 / bwa etc
# - Based on a script available from the HISAT2 website
# 
###############################################################################

export EXT_DATA_DIR="./data/ext"
export OUTDIR_PREFIX="${EXT_DATA_DIR}/genomes"

export GENOME_ID="GRCh38"
export HISAT_BASENAME="grch38_tran"
export species="homo_sapiens"
export Species="Homo_sapiens"
export ENSEMBL_RELEASE="84"

bash ./scripts/reference_downloader.sh

###############################################################################
