#!/bin/bash -e

###############################################################################
## 14/11/2016
## - Script to download the mouse-specific genome / transcriptome assemblies
##   and references for use in hisat2 / bwa etc
## - Based on a script available from the HISAT2 website
## 
###############################################################################

export EXT_DATA_DIR="./data/ext"
export OUTDIR_PREFIX="${EXT_DATA_DIR}/genomes"

export GENOME_ID="GRCm38"
export HISAT_BASENAME="grcm38_tran"
export species="mus_musculus"
export Species="Mus_musculus"
export ENSEMBL_RELEASE="84"

bash ./scripts/reference_downloader.sh

###############################################################################
