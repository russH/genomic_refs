#!/bin/bash -e

input_tarbz="${1}"
output_tarbz="${2}"

if ! [[ "${input_tarbz}" = *.tar.bz2 ]]; then
  echo "input (first arg) was not a tar.bz2"
  exit 1
  fi

if ! [[ -f "${input_tarbz}" ]]; then
  echo "input file ($1) does not exist";
  exit 1
  fi

if ! [[ "${output_tarbz}" = *.tar.bz2 ]]; then
  echo "output (second arg) was not a tar.bz2"
  exit 1
  fi

# make a directory named {output...} where output_tarbz=={output...}.tar.bz2
output_dir=$(dirname "${output_tarbz}")/$(basename "${output_tarbz}" .tar.bz2)
mkdir -p "${output_dir}"

# temporarily bunzip2 the input
input_tar=$(dirname "${input_tarbz}")/$(basename "${input_tarbz}" .bz2)
bunzip2 --keep "${input_tarbz}"

# extract all filepaths from input .tar
# iterate over the files within the .tar
#   extract the top 100 lines from each into {output...}
for fname in $(tar --list -f "${input_tar}");
do
  if [[ "${fname}" = */ ]];
    then mkdir -p "${output_dir}/${fname}"
    continue
    fi

  tar --to-stdout -xf "${input_tar}" "${fname}" |\
    head -n101 |\
    tail -n100 \
    > "${output_dir}/${fname}"
done

# tar & compress the output_dir as if tarring from inside that dir
tar --directory "${output_dir}" -cjf "${output_tarbz}" .

# remove the output_dir
rm -R "${output_dir}"

# remove the uncompressed version of the input
rm "${input_tar}"
