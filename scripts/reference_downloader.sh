#!/bin/bash -e

###############################################################################
# 2017-07-20
#
# - Script to download the genome / transcriptome assemblies and references for
#   use in hisat2 / bwa etc
# - Based on a script available from the HISAT2 website
# 
###############################################################################

# Inherit these from ENV if they already exist & die if they don't exist

# Variables that should be defined in the calling script:
# GENOME_ID
# HISAT_BASENAME
# OUTDIR_PREFIX
# species
# Species
# ENSEMBL_RELEASE

if [ -z "${GENOME_ID}" ];
  then echo "GENOME_ID should be defined" >&2; exit 2;
  fi
if [ -z "${HISAT_BASENAME}" ];
  then echo "HISAT_BASENAME should be defined" >&2; exit 2;
  fi
if [ -z "${OUTDIR_PREFIX}" ];
  then echo "OUTDIR_PREFIX should be defined" >&2; exit 2;
  fi
if [ -z "${species}" ];
  then echo "species should be defined" >&2; exit 2;
fi
if [ -z "${Species}" ];
  then echo "Species should be defined" >&2; exit 2;
fi
if [ -z "${ENSEMBL_RELEASE}" ];
  then echo "ENSEMBL_RELEASE should be defined" >&2; exit 2;
fi

###############################################################################

OUTDIR="${OUTDIR_PREFIX}/${GENOME_ID}"
HISAT_OUTDIR="${OUTDIR}/hisat2"
ASSEMBLY_OUTDIR="${OUTDIR}/assembly"

HISAT_URL_PREFIX="ftp://ftp.ccb.jhu.edu/pub/infphilo/hisat2/data"
HISAT_URL="${HISAT_URL_PREFIX}/${HISAT_BASENAME}.tar.gz"

ENSEMBL_PREFIX="ftp://ftp.ensembl.org/pub/release-${ENSEMBL_RELEASE}"

ASSEMBLY_FA_PREFIX="${ENSEMBL_PREFIX}/fasta/${species}/dna"
ASSEMBLY_FA_BASENAME="${Species}.${GENOME_ID}.dna.primary_assembly"
ASSEMBLY_FA_URL="${ASSEMBLY_FA_PREFIX}/${ASSEMBLY_FA_BASENAME}.fa.gz"
ASSEMBLY_FA_OUT_NOEXT="${ASSEMBLY_OUTDIR}/${ASSEMBLY_FA_BASENAME}"
ASSEMBLY_FA_OUT="${ASSEMBLY_FA_OUT_NOEXT}.fa.gz"
ASSEMBLY_DICT="${ASSEMBLY_FA_OUT_NOEXT}.dict"

###############################################################################

echo "Checking dependencies in ${0}" >&2

DEPENDENCIES=("curl" \
              "picard" \
              "bwa" \
              "bgzip" \
              "samtools")

for prog in "${DEPENDENCIES[@]}";
do
  if [[ -z `which ${prog}` ]]
  then
    echo "Dependency ${prog} is missing in script ${0}" >&2
    exit 1
  fi
done

###############################################################################

# Check that the base directory for all downloads / files is valid
# - then set up subdirectories if it is

echo "Checking existing directories in ${0}" >&2

if [[ ! -d "${OUTDIR_PREFIX}" ]]; then
  echo "OUTDIR_PREFIX: ${OUTDIR_PREFIX} should be a valid directory in" `basename ${0}`
  exit 1
  fi

# Make these dirs if they don't exist:

REQUIRED_DIRS=("${OUTDIR}" \
               "${HISAT_OUTDIR}" \
               "${ASSEMBLY_OUTDIR}")

for DNAME in "${REQUIRED_DIRS[@]}"
do
  if [[ ! -d "${DNAME}" ]]; then
    mkdir "${DNAME}"
    fi
done

###############################################################################
# Transcriptome reference - for use in HISAT2
# --
# The hisat references look like ./path/to/GRCm38/hisat2/genome_tran.[1-8].ht2
#   and a further make_grcm38_tran.sh file is also in the tar.gz downloaded
#   from the hisat2 ftp site
# We check for an example HISAT file, if it is missing, we download the hisat
#   reference again

if [[ ! -f "${HISAT_OUTDIR}/genome_tran.1.ht2" ]]; then
  echo "Downloading HISAT2 transcriptome reference" >&2
  # Download the archive
  curl "${HISAT_URL}" > "${OUTDIR}/hisat2.tar.gz"
  # Extract into the directory ${OUTDIR}/${HISAT_BASENAME}
  tar --directory ${OUTDIR} \
      -xzvf "${OUTDIR}/hisat2.tar.gz"
  # Rename the extracted directory for consistency between different GENOME_IDs
  mv -T "${OUTDIR}/${HISAT_BASENAME}" \
        "${HISAT_OUTDIR}"
  # Remove the archive
  rm "${OUTDIR}/hisat2.tar.gz"
  fi

###############################################################################
# Transcriptome definition from Ensembl should be downloaded using a bespoke
# snakemake rule; see scripts/snake_recipes/ensembl_gtf.smk

###############################################################################

# Genome definition from Ensembl
# --
if [[ ! -f "${ASSEMBLY_FA_OUT}" ]]; then
  echo "Downloading the genome assembly: ${ASSEMBLY_FA_OUT}" >&2
  # Download the ensembl-website version of the genome and ensure it is
  # compressed with `bgzip` (for quick access within samtools)

  # <TODO>: The .fa.gz file from ensembl is not bgzipped, it is just gzipped
  #   therefore, workout how to download and bgzip it:
  #   `curl -s` is used to drop the progress bar and other stdout-put
  curl -s "${ASSEMBLY_FA_URL}" |\
    gunzip - |\
    bgzip > "${ASSEMBLY_FA_OUT}"
  fi

# Use samtools faidx to index the bare .fa.gz file
if [[ ! -f "${ASSEMBLY_FA_OUT}.fa.gz.fai" ]]; then
  samtools faidx "${ASSEMBLY_FA_OUT}"
  fi

# Index the assembly using bwa
# - Test using just one of the index files
if [[ ! -f "${ASSEMBLY_FA_OUT_NOEXT}.bwt" ]]; then
  echo "Indexing the genome assembly" >&2
  bwa index -p "${ASSEMBLY_FA_OUT_NOEXT}" \
               "${ASSEMBLY_FA_OUT}"
  fi

# Make a sequence dictionary for the assembly
if [[ ! -f "${ASSEMBLY_DICT}" ]]; then
  echo "Creating the sequence dictionary for the assembly" >&2
  picard CreateSequenceDictionary \
    REFERENCE="${ASSEMBLY_FA_OUT}" \
    OUTPUT="${ASSEMBLY_DICT}"
  fi

###############################################################################
