# Download a .gtf defining an ensembl transcriptome build
# - The (external) gtf should be specified in the config file for this
# snakemake workflow

def ensembl_gtf_input(wildcards):
    """
    Obtain the FTP location for the Ensembl-GTF that is defined in the
    snakemake config file.
    """
    return FTP.remote(
        config[wildcards.genome_build]["ensembl"][wildcards.ensembl_build]["ftp"],
        keep_local=False, immediate_close=True
    )

rule ensembl_gtf:
    message:
        """
        --- Download an Ensembl transcriptome definition from FTP

            input: {input}
            output: {output}
        """

    input:
        ensembl_gtf_input

    output:
        os.path.join(
            genome_dir,
            "{genome_build}",
            "ensembl.v{ensembl_build}",
            "{species}.{genome_build}.{ensembl_build}.gtf.gz"
        )

    wildcard_constraints:
        # eg, GRCh38
        genome_build="GRC[mh]\d+",
        # eg, 84
        ensembl_build="\d+",
        # eg, mus_musculus
        species="[a-z]+_[a-z]+"

    shell:
        """
        mv {input} {output}
        """

###############################################################################
