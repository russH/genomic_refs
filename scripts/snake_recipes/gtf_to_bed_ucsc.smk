# takes a gtf file
# converts it to a 12-column BED file, for use in RSeQC
# only keeps protein-coding genes

rule gtf_to_protein_coding_bed:
    message:
        """
        --- Make a 12-column `.bed` file based on a `.gtf` transcriptome
            definition. Only protein-coding transcripts are included in the
            output.

            input: {input}
            output: {output}
        """

    input:
        gtf = "{prefix}.gtf.gz"

    output:
        bed = "{prefix}.protein_coding.bed12"

    shell:
        """
        cat {input} |\
            gunzip - |\
            grep "protein_coding" |\
            gtfToGenePred /dev/stdin /dev/stdout |\
            genePredToBed /dev/stdin {output}
        """

